# palmistry

Playing around with PaLM (Google API for generative AI).
At this point the `hey` command is probably most interesting thing here.
For that, check out the *Setup* section and then the *hey* section for that.
An example of using this command after you get set up:

Command:
    hey what is the pandoc command to convert DOCX to HTML

Response:
    $ pandoc -f docx -t html input.docx -o output.html

* Started 4 August 2023 (LMK)

## Setup

Install (or if installed upgrade) Google generative AI python library:

    pip install -U google-generativeai

You need to supply your own PaLM API key for any of this to work.
Create a file named `secret_config.py` (in util/) containing the declaration:

    PaLM_API_key = "yourKeyGoesHere"

API key generation is done at the following page under your Google account
(you may need to sign up on a waiting list in advance).
https://makersuite.google.com/app/home

Configuration for app secrets are excluded from the codebase
(using .gitignore entry secret_*).

## hey

`hey` is a little command line tool for querying the PaLM API to get help
on linux commands or other technical questions.
(It puts your question in that context with a prompt but you can ask anything.)

Since python packaging is hard, just run it from the python files in `util/`.
There is an executable shim `hey` to invoke the command;
include it in your shell path, if you copy it fixup the directory references.

Known issues: the prompt building for `hey` is very primitive.
For example, it asks for responses without markdown but that still happens.

## palmy

There is a command line tool called `palmy.py`in the /util directory;
-- try `--help` for details.
(NOTE: in the following, `$` indicates command line.)

Simple command line tool invoking the API (here is a simple test case):

    $ python3 -m palmy test
    0.7314058140930012 0.4283014213879995 0.46812476127099983
    0.9999988160990008 0.9999990604709993 0.9999999108279983

Here is a mode that compares embeddings vectors for (dot product) similarity.
Run the following command and type or pipe text lines into stdin.

    $ python3 -m palmy lines

This produces a listing of lines with a number indicating vector alignment.

    1: 0.0000000|This is the first line
    2: 0.7288087|And second line ...
    Best similarity pair: 1 and 2 (0.7288087)

*(the above is for explanatory purposes, not real output)*

The numbers on the left (1, 2) are line numbers, followed by the similarity
number (negative indicates opposite direction) for the preceding line
(the first is 0 because there is no preceding line).
Blank lines indicate paragraph breaks and are skipped (so the line
numbers will jump); it's common to see lower similarity at paragraph breaks.

The last output line tells you the two lines that are most similar.

The command line verb to generate text response to a prompt is 'prompt'.
Here is an example of a one line text prompt (the ^D is end of file ctrl-D).

    $ python3 -m palmy prompt
    What is the meaning of life in 25 words or less?
    ^D
    To live, to love, to learn, to leave your mark.


## Caching

Since the PaLM API call is slow (about one second) there is a cache file
that memorizes embeddings for unique text strings (matched by a hash).
If you calculate the same text embedding and there is a cache match it
will just use the same result instead of doing the API call. This speeds
up things for repeated test runs greatly.

The default cache file path is `/tmp/_palm_cache` (or specify `-c` argument)
and you can wipe the cache before the run with `-w` flag.
If you don't want caching `-c /dev/null` disables it entirely

Generating text usually has a non-zero temperature so there is no caching.
