"""Simple command line invocation of PaLM API for research.
Command line:
    python3 -m palmy -v test
    python3 -m palmy lines # provide stdin text lines to process
    python3 -m palmy prompt # provide stdin text prompt to get response

Notes: output for the 'test' invocation:
0.7314062496236002 0.42830106333609075 0.46812422414244004
Embedding API is pretty fast, about one second per invocation.
Reference
https://developers.generativeai.google/tutorials/embeddings_quickstart
"""


__author__ = "Loren Kohnfelder"
__copyright__ = "Copyright 2023 Loren Kohnfelder"
__version__ = "0"
__license__ = "CC BY-SA 4.0"


from palmer import Palmer, dot_product
import argparse
import sys


def test_run(args, palmer):
    """Do a test run with the toy example in the API docs (modified).
    """
    base = 'What do squirrels eat?'
    niteru = 'nuts and acorns'
    chigau = 'This morning I went for a walk in the rain.'
    # Create an embedding
    palmer.initialize('embedText', verbose=args.verbose)
    embed_base = palmer.embedding(base)
    embed_niteru = palmer.embedding(niteru)
    embed_chigau = palmer.embedding(chigau)
    with open('/tmp/palmy.log', 'w') as log:
        embeds = {'base': embed_base, 'niteru': embed_niteru,
                  'chigau': embed_chigau}
        print(repr(embeds), file=log)
    similar = dot_product(embed_base['embedding'], embed_niteru['embedding'])
    unsim1 = dot_product(embed_base['embedding'], embed_chigau['embedding'])
    unsim2 = dot_product(embed_niteru['embedding'], embed_chigau['embedding'])
    print(similar, unsim1, unsim2)
    x = dot_product(embed_base['embedding'], embed_base['embedding'])
    y = dot_product(embed_niteru['embedding'], embed_niteru['embedding'])
    z = dot_product(embed_chigau['embedding'], embed_chigau['embedding'])
    print(x, y, z)


def lines_run(args, palmer):
    """Do a run with lines of text from stdin, and show embedding
    relationships. Empty lines for paragraph breaks and skipped.
    """
    palmer.initialize('embedText', verbose=args.verbose)
    textlines = []
    textembeds = []
    previous_embed = None
    lineno = 1
    for line in sys.stdin:
        line = line.strip()
        if line:
            embed = palmer.embedding(line)['embedding']
            if previous_embed:
                previous_similarity = dot_product(previous_embed, embed)
            else:
                previous_similarity = 0
            print("%5d:%10.7f|%s" % (lineno, previous_similarity, line[:40]))
        textlines.append(line)
        textembeds.append(embed)
        previous_embed = embed
        lineno += 1

    best = (None, None, -9999)
    for i in range(len(textembeds)):
        if textlines[i]:
            for j in range(i+1, len(textembeds)):
                if textlines[j]:
                    ij_similarity = dot_product(textembeds[i], textembeds[j])
                    if ij_similarity > best[2]:
                        best = (i, j, ij_similarity)
    print("Best similarity pair: %d and %d (%f)" % best)


def prompt_run(args, palmer):
    """Do a run generating text from a prompt that is read from stdin.
    """
    palmer.initialize('generateText', safety=not args.unsafe,
                      verbose=args.verbose)
    if args.verbose:
        print(repr(palmer.safety))
    lines = sys.stdin.readlines()
    prompt = "".join(lines)
    response = palmer.generateResponse(prompt)
    print(response)


def palmy(args):
    """Top level invocation under command line direction.
    """
    palmer = Palmer()
    if args.wipe:
        palmer.clear_cache()
    else:
        cn = palmer.load_cache(args.cache)
    if args.verbose:
        print("Pre-cached %d (%s)" % (cn, args.cache))
        print(palmer.cache_info())

    if args.action == "lines":
        lines_run(args, palmer)
    elif args.action == "test":
        test_run(args, palmer)
    elif args.action == "prompt":
        prompt_run(args, palmer)

    cn = palmer.save_cache(args.cache)
    if args.verbose:
        print("Post-cached %s (%s)" % (palmer.cache_info(), args.cache))


def main(argv=None):
    """Parse command line arguments and invoke processing.
    Run with --help for full options.
    """
    parser = argparse.ArgumentParser(
        description="Simple invocation of PaLM API for research",
        epilog=("Version: %s" % __version__))
    parser.add_argument('-c', '--cache', help="cache file pathname",
                        action='store', default="/tmp/_palm_cache")
    parser.add_argument('-u', '--unsafe', help="disable all safety controls",
                        action='store_true')
    parser.add_argument('-v', '--verbose', help="verbose output mode",
                        action='store_true')
    parser.add_argument('-w', '--wipe', help="wipe the cache clear",
                        action='store_true')
    parser.add_argument('action', nargs='?', default='test',
                        choices=['lines', 'test', 'prompt'],
                        help="Action to take")
    args = parser.parse_args(argv)
    if (args.verbose):
        print("Invocation args: " + repr(args))
    palmy(args)


if __name__ == '__main__':
    main(sys.argv[1:])
