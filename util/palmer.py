"""Simple wrapper for Google PaLM API library.
Reference about the PaLM API link
https://developers.generativeai.google/models/language
https://developers.generativeai.google/api
Notes:
* a good test prompt for the safety: What is the worst thing about being alive?
"""


__author__ = "Loren Kohnfelder"
__copyright__ = "Copyright 2023 Loren Kohnfelder"
__version__ = "0"
__license__ = "CC BY-SA 4.0"


import secret_config
import google.generativeai as palm
from google.generativeai.types import safety_types
import hashlib
import os
import sys


class Palmer:

    def __init__(self, model="models/embedding-gecko-001"):
        """Object creation can specify a prefered model or default is used.
        By default no caching happens unless you clear or load the cache.
        """
        self.threshold = safety_types.HarmBlockThreshold.BLOCK_LOW_AND_ABOVE
        self.model = model
        self.api_key = secret_config.PaLM_API_key
        self.cache = None

    def load_cache(self, pathname):
        """Load in-memory cache from the pathname to optimize subsequent work.
        File consists of text lines of the form:
        0123456789abcdef:0.1, 0.2, ... (many floats)
        Returns number of cache entries.
        """
        self.cache = {}
        if not os.path.isfile(pathname):
            return 0
        with open(pathname, "r") as f:
            for line in f:
                line = line.strip()
                if line:
                    key, values = line.split(":", 1)
                    values = [float(x) for x in values.split(",")]
                    self.cache[key] = values
        return len(self.cache)

    def clear_cache(self):
        """Clear the cache to re-evaluate with the API.
        """
        self.cache = {}

    def check_cache(self, text_key):
        """Check whether key for a text is in the cache.
        Returns the pair (cache key, cached values if present or None if not),
        or None if there is no caching.
        """
        if self.cache is not None:
            key = hex_sha1(text_key)
            if key in self.cache:
                return (key, self.cache[key])
            return (key, None)
        return None

    def put_cache(self, key, values):
        """Put the values into the cache for the given key.
        This is the key returned as first element of the pair from check_cache.
        """
        self.cache[key] = values

    def cache_info(self):
        """Return a string with info about the current cache
        (number of entries and byte size) or None if no cache.
        """
        if self.cache:
            size = sys.getsizeof(self.cache)
            for key in self.cache:
                size += sys.getsizeof(key)
                size += sys.getsizeof(self.cache[key])
            return "%d (%s bytes)" % (len(self.cache), format(size, ','))
        return None

    def save_cache(self, pathname):
        """Save the in-memory cache to a file for use next invocation.
        Use load_cache method to reload and continue using it.
        Returns number of cache entries.
        """
        with open(pathname, "w") as f:
            for key in self.cache:
                numbers = []
                for num in self.cache[key]:
                    numbers.append("%f" % num)
                print("%s:%s" % (key, ",".join(numbers)), file=f)
        return len(self.cache)

    def initialize(self, kind, safety=True, verbose=False):
        """Initializes the API, returns True if ready or False if broken.
        The caller must specify the kind of model required:
        'embedText' or 'generateText'
        The safety parameter can be false to turn off all safety controls.
        """
        if not safety:
            self.threshold = safety_types.HarmBlockThreshold.BLOCK_NONE
        self.safety = [
            {"category": safety_types.HarmCategory.HARM_CATEGORY_DEROGATORY,
             "threshold": self.threshold},
            {"category": safety_types.HarmCategory.HARM_CATEGORY_TOXICITY,
             "threshold": self.threshold},
            {"category": safety_types.HarmCategory.HARM_CATEGORY_VIOLENCE,
             "threshold": self.threshold},
            {"category": safety_types.HarmCategory.HARM_CATEGORY_SEXUAL,
             "threshold": self.threshold},
            {"category": safety_types.HarmCategory.HARM_CATEGORY_MEDICAL,
             "threshold": self.threshold},
            {"category": safety_types.HarmCategory.HARM_CATEGORY_DANGEROUS,
             "threshold": self.threshold},
        ]
        palm.configure(api_key=self.api_key)
        models = palm.list_models()
        has_models = list()
        for model in models:
            if kind in model.supported_generation_methods:
                has_models.append(model.name)
        if verbose:
            print("Models: " + repr(has_models))
        if 0 == len(has_models):
            return False
        if self.model not in has_models:
            self.model = has_models[0]
        if verbose:
            print("Using model: " + self.model)
        return True

    def embedding(self, text):
        """Compute and return the embedding for the text.
        Returns {'embedding': [-0.1, -0.025, ... ]}
        NOTE that the cache is just the numbers
        """
        result = self.check_cache(text)
        if result:
            values = result[1]
            if values:
                return {'embedding': values}
        embed = palm.generate_embeddings(model=self.model, text=text)
        if result:
            self.put_cache(result[0], embed['embedding'])
        return embed

    def generateResponse(self, prompt, temp=0, max_tokens=1000):
        """Compute and return the generated text from a response.
        Parameters for temperature (temp) and output token limit (max_tokens)
        may be provided. Returns the generated text as a string.
        """
        completion = palm.generate_text(model=self.model,
                                        prompt=prompt,
                                        safety_settings=self.safety,
                                        temperature=temp,
                                        max_output_tokens=400)
        response = completion.result
        if response is None:
            response = "BLOCKED (%s | %s)" % (repr(completion.filters),
                                              repr(completion.safety_feedback))
        return response


def dot_product(v1, v2):
    """Computes the dot product of two one dimensional vectors.
    Returns the dot product of the two vectors, or 0 if sizes differ.
    """
    if len(v1) != len(v2):
        return 0
    sum = 0
    for i in range(len(v1)):
        sum += v1[i] * v2[i]
    return sum


def hex_sha1(text):
    """Computes the SHA-1 hash of a string.
    Returns the SHA-1 hash of the text as a hex string.
    """
    hash_object = hashlib.sha1()
    hash_object.update(text.encode("utf-8"))
    return hash_object.hexdigest()
