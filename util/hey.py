"""A command line tool to ask quick questions via the PaLM API.
You need an API key of your own for this to work (declare in secret_config.py)
PaLM_API_key = "your own key"
Get your key here:
https://developers.generativeai.google/tutorials/setup
For limitations of the model see:
https://developers.generativeai.google/models/language
"""


__author__ = "Loren Kohnfelder"
__copyright__ = "Copyright 2023 Loren Kohnfelder"
__version__ = "0"
__license__ = "CC BY-SA 4.0"


from palmer import Palmer
import argparse
import os
from pathlib import Path
import sys
import textwrap
import tomllib


def linux_info():
    osname = "Linux"
    osversion = ""
    with open("/etc/os-release", "rb") as f:
        osinfo = tomllib.load(f)
        if 'NAME' in osinfo:
            osname = osinfo['NAME']
        if 'VERSION' in osinfo:
            osversion = " " + osinfo['VERSION']
    return osname + osversion


HEY_DEFAULTS = {
    'BASE': "What follows is a brieft query by a programmer for your help. "
    "In your response, do not use markdown syntax; assume that the person"
    " asking can figure out the meaning without. Precede commands with '$ '.",
    'TOPIC': "Unless otherwise stated, assume the query is asking for"
    " a Linux command; however, an explicit topic may be specified by an"
    " initial word or words followed by a colon (e.g. 'JavaScript:').",
    'LENGTH': "Unless otherwise stated, concise answers are preferred.",
    'EXAMPLES': "Often good answers include examples; please provide these"
    " at the top of your answer followed by '=====' separator for clarity.",
    'EXPERTISE': "Unless otherwise stated, assume the person is familiar"
    " with the answer and doesn't need lots of explanation, keep comments"
    " to a minimum.",
    'SAFETY': "",
    'UNSAFETY': "",
    'INTRO': "Remember not to use markdown or other demarcations unless asked."
    "Given the preceding context, the person's query follows: "
}

PROMPT_PARTS = ['BASE', 'TOPIC', 'LENGTH', 'EXAMPLES', 'EXPERTISE']


def get_param(paramstack, key, default=None):
    """Get parameter from a stack of dictionaries, trying them in order.
    """
    for params in paramstack:
        if key in params:
            return params[key]
    return default


def build_prompt(prompt, unsafe=False, verbose=False):
    """Prompts are built from several strings provided by parameter dicts.
    There are written as TOML files and backed by HEY_DEFAULTS in the code.
    In order of precedence, these files are ".hey" in:
    the current directory, the home directory, the code directory (of hey.py).
    The prompt will be a catenation of the strings keyed by PROMPT_PARTS.
    Optional additional prompt may be given depending on safety setting,
    using SAFETY or UNSAFETY.
    Following all this prologue is the INTRO phrase followed by the query.
    """
    paramstack = [HEY_DEFAULTS]
    codepath = Path(__file__).resolve()
    codedir = codepath.parent
    path = codedir / ".hey"
    if os.path.isfile(path):
        with open(path, "rb") as f:
            codedir_hey = tomllib.load(f)
            paramstack.insert(0, codedir_hey)
    homedir = Path.home()
    if homedir != codedir:
        path = homedir / ".hey"
        if os.path.isfile(path):
            with open(path, "rb") as f:
                homedir_hey = tomllib.load(f)
                paramstack.insert(0, homedir_hey)
    currdir = Path.cwd()
    if currdir != homedir and currdir != codedir:
        path = currdir / ".hey"
        if os.path.isfile(path):
            with open(path, "rb") as f:
                currdir_hey = tomllib.load(f)
                paramstack.insert(0, currdir_hey)
    preface = ""
    for key in PROMPT_PARTS:
        str = get_param(paramstack, key)
        preface = preface + (" " if preface else "") + str
    key = 'UNSAFETY' if unsafe else 'SAFETY'
    str = get_param(paramstack, key)
    preface = preface + (" " if preface else "") + str
    str = get_param(paramstack, 'INTRO')
    preface = preface + (" " if preface else "") + str
    str = preface + (" " if preface else "") + prompt
    str = textwrap.wrap(str, width=72, break_on_hyphens=False)
    return '\n'.join(str)


def query(prompt, unsafe=False, verbose=False):
    """Query the LLM with the command line string.
    """
    palmer = Palmer()
    palmer.initialize('generateText', safety=not unsafe, verbose=verbose)
    response = palmer.generateResponse(prompt)
    return response


def hey(args):
    """Top level invocation under command line direction.
    """
    prompt = ' '.join(args.query)
    full_prompt = build_prompt(prompt,
                               unsafe=args.unsafe, verbose=args.verbose)
    if args.verbose and not args.dryrun:
        print("Q: " + full_prompt)
    if args.dryrun:
        print(repr({'unsafe': args.unsafe, 'verbose': args.verbose}))
        response = full_prompt
    else:
        response = query(full_prompt, unsafe=args.unsafe, verbose=args.verbose)
    print(response)


def main(argv=None):
    """Parse command line arguments and invoke processing.
    Run with --help for full options.
    """
    parser = argparse.ArgumentParser(
        description="Ask quick questions about Linux via the PaLM API.",
        epilog=("Version: %s" % __version__))
    parser.add_argument('-n', '--dryrun',
                        help="dry run doesn't actually ask the API, for tests",
                        action='store_true')
    parser.add_argument('-u', '--unsafe', help="disable all safety controls",
                        action='store_true')
    parser.add_argument('-v', '--verbose', help="verbose output mode",
                        action='store_true')
    parser.add_argument('query', nargs='+',
                        help="Text of your query")
    args = parser.parse_args(argv)
    if (args.verbose):
        print("Invocation args: " + repr(args))
    hey(args)


if __name__ == '__main__':
    main(sys.argv[1:])
